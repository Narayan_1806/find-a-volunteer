INSERT INTO roles(Name, createdAt, updatedAt) VALUES('Staff', NOW(), NOW());

INSERT INTO roles(Name, createdAt, updatedAt) VALUES('Volunteer', NOW(), NOW());

-- Change this to default location
INSERT INTO locations(Name,State, createdAt, updatedAt) VALUES('Default', 'Default',NOW(), NOW());

-- All major cities
INSERT INTO locations(Name,State, createdAt, updatedAt) VALUES('Hyderabad', 'Telangana',NOW(), NOW());

INSERT INTO locations(Name,State, createdAt, updatedAt) VALUES('Amritsrar', 'Punjab',NOW(), NOW());

INSERT INTO locations(Name,State, createdAt, updatedAt) VALUES('Mumbai', 'Maharashtra',NOW(), NOW());

INSERT INTO locations(Name,State, createdAt, updatedAt) VALUES('New Delhi', 'Delhi',NOW(), NOW());

INSERT INTO locations(Name,State, createdAt, updatedAt) VALUES('Bangalore', 'Karnataka',NOW(), NOW());

INSERT INTO locations(Name,State, createdAt, updatedAt) VALUES('Ahmedabad', 'Gujarat',NOW(), NOW());

INSERT INTO locations(Name,State, createdAt, updatedAt) VALUES('Chennai', 'Tamil Nadu',NOW(), NOW());

INSERT INTO locations(Name,State, createdAt, updatedAt) VALUES('Jaipur', 'Rajasthan',NOW(), NOW());

INSERT INTO locations(Name,State, createdAt, updatedAt) VALUES('Lucknow', 'Uttar Pradesh',NOW(), NOW());

INSERT INTO locations(Name,State, createdAt, updatedAt) VALUES('Patna', 'Bihar',NOW(), NOW());

INSERT INTO locations(Name,State, createdAt, updatedAt) VALUES('Bhubaneshwar', 'Orissa',NOW(), NOW());

INSERT INTO locations(Name,State, createdAt, updatedAt) VALUES('Bhopal', 'Madhya Pradesh',NOW(), NOW());

-- Change this with default users location
INSERT INTO findavolunteer.users(Username, Email, PhoneNumber, Password, RoleId,
createdAt, updatedAt) VALUES('Default Staff','madirajupriyanka@gmail.com', '88988888', 'Priyanka',1, NOW(), NOW());

-- Change this to default location
INSERT INTO  findavolunteer.staffs(LocationID,createdAt, updatedAt, userId) 
VALUES(1, NOW(), NOW(), 1);