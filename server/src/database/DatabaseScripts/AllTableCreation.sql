USE FindAVolunteer;

CREATE TABLE IF NOT EXISTS Services
(
	ID MEDIUMINT NOT NULL AUTO_INCREMENT,
    Name VARCHAR(300) NOT NULL,
    CreatedDateTime DATETIME,
	LastModifiedDateTime DATETIME,
    PRIMARY KEY(ID)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS Organizations
(
	ID MEDIUMINT NOT NULL AUTO_INCREMENT,
    Type VARCHAR(200),
	Budget DECIMAL(15,2),
	Description TEXT,
	StaffSize INTEGER,
	ServiceId MEDIUMINT,
	CreatedDateTime DATETIME,
	LastModifiedDateTime DATETIME,
	FOREIGN KEY (ServiceId)
        REFERENCES Services(ID)
        ON DELETE CASCADE,
	PRIMARY KEY(ID)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS ‎Locations 
(
	ID MEDIUMINT NOT NULL AUTO_INCREMENT,
    LocName VARCHAR(600) NOT NULL,
	StateName VARCHAR(600) NOT NULL,
	CreatedDateTime DATETIME,
	LastModifiedDateTime DATETIME,
    PRIMARY KEY(ID)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS Roles
(
	ID MEDIUMINT NOT NULL AUTO_INCREMENT,
	RoleName VARCHAR(100),
    CreatedDateTime DATETIME,
	LastModifiedDateTime DATETIME,
    PRIMARY KEY(ID)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS Users
(
	ID MEDIUMINT NOT NULL AUTO_INCREMENT,
	Username VARCHAR(350),
    Email VARCHAR(320) NOT NULL,
    Password TEXT,
    ContactNum VARCHAR(20) NOT NULL,
    RoleId MEDIUMINT,
    IsEmailVerified BIT,
	IsGoogleAuth BIT,
	IsActive BIT,
	NumberOfPastEvents INTEGER,
    NumberOfCurrentEvents INTEGER,
    CreatedDateTime DATETIME,
	LastModifiedDateTime DATETIME,
	FOREIGN KEY (RoleId)
        REFERENCES Roles(ID)
        ON DELETE CASCADE,
	PRIMARY KEY(ID)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS StaffDetails
(
	ID MEDIUMINT NOT NULL AUTO_INCREMENT,
    UserID MEDIUMINT NOT NULL,
    HeadOfLocation MEDIUMINT NOT NULL,
    OrganizationId MEDIUMINT NOT NULL,
	CreatedDateTime DATETIME,
	LastModifiedDateTime DATETIME,
	FOREIGN KEY (OrganizationId)
        REFERENCES Organizations(ID)
        ON DELETE CASCADE,
	FOREIGN KEY (UserID)
        REFERENCES Users(ID)
        ON DELETE CASCADE,
    PRIMARY KEY(ID)
) ENGINE = INNODB;


CREATE TABLE IF NOT EXISTS VolunteerDetails
(
	ID MEDIUMINT NOT NULL AUTO_INCREMENT,
    UserID MEDIUMINT,
    StaffId MEDIUMINT,
    IsPromoted BIT,
    AvgRating FLOAT(2,2),
    PreferredDays INTEGER,
    PreferredStartTime TIME,
    PreferredEndTime TIME,
    WorkExperience TEXT,
    TypeOfVolunteeringInterested INTEGER,
	CreatedDateTime DATETIME,
	LastModifiedDateTime DATETIME,
	FOREIGN KEY (StaffId)
        REFERENCES StaffDetails(ID)
        ON DELETE CASCADE,
	FOREIGN KEY (UserID)
        REFERENCES Users(ID)
        ON DELETE CASCADE,
    PRIMARY KEY(ID)
) ENGINE = INNODB;


CREATE TABLE IF NOT EXISTS SkillCategories
(
	ID MEDIUMINT NOT NULL AUTO_INCREMENT,
	CategoryName VARCHAR(300) NOT NULL,
    CreatedDateTime DATETIME,
	LastModifiedDateTime DATETIME,
    PRIMARY KEY(ID)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS Skills
(
	ID MEDIUMINT NOT NULL AUTO_INCREMENT,
	SkillName VARCHAR(350) NOT NULL,
    CreatedDateTime DATETIME,
	LastModifiedDateTime DATETIME,
    PRIMARY KEY(ID)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS VolunteerSkillMapping
(
	ID MEDIUMINT NOT NULL AUTO_INCREMENT,
    VolunteerID MEDIUMINT,
	SkillID MEDIUMINT,
	CreatedDateTime DATETIME,
	LastModifiedDateTime DATETIME,
	PRIMARY KEY(ID),
     FOREIGN KEY (SkillID)
        REFERENCES Skills(ID)
        ON DELETE CASCADE,
	FOREIGN KEY (VolunteerID)
        REFERENCES VolunteerDetails(ID)
        ON DELETE CASCADE
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS EventStatus
(
	ID MEDIUMINT NOT NULL AUTO_INCREMENT,
	EventStatus VARCHAR(100),
    CreatedDateTime DATETIME,
	LastModifiedDateTime DATETIME,
    PRIMARY KEY(ID)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS Events
(
	ID MEDIUMINT NOT NULL AUTO_INCREMENT,
    Description TEXT,
    LocationId MEDIUMINT,
    StartDateTime DATETIME,
    EndDateTime DATETIME,
    NumOfVolunteersNeeded INTEGER,
    Impact TEXT,
    EventStatusId MEDIUMINT,
    StaffDetailId MEDIUMINT,
    RejectedReason TEXT,
    AcceptedMessage TEXT,
    ShouldBeFinalizedIn INTEGER,
    CreatedDateTime DATETIME,
	LastModifiedDateTime DATETIME,
    PRIMARY KEY(ID),
	FOREIGN KEY (EventStatusId)
        REFERENCES EventStatus(ID)
        ON DELETE CASCADE,
	FOREIGN KEY (StaffDetailId)
        REFERENCES StaffDetails(ID)
        ON DELETE CASCADE
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS EventApplications 
(
	ID MEDIUMINT NOT NULL AUTO_INCREMENT,
    EventID MEDIUMINT,
    VolunteerID MEDIUMINT,
	Comments TEXT,
    CreatedDateTime DATETIME,
	LastModifiedDateTime DATETIME,
    PRIMARY KEY(ID),
    FOREIGN KEY (EventID)
        REFERENCES Events(ID)
        ON DELETE CASCADE,
	FOREIGN KEY (VolunteerID)
        REFERENCES VolunteerDetails(ID)
        ON DELETE CASCADE
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS EventIncludedVolunteers 
(
	ID MEDIUMINT NOT NULL AUTO_INCREMENT,
	EventID MEDIUMINT,
    VolunteerID MEDIUMINT,
    CreatedDateTime DATETIME,
	LastModifiedDateTime DATETIME,
	PRIMARY KEY(ID),
    FOREIGN KEY (EventID)
        REFERENCES Events(ID)
        ON DELETE CASCADE,
	FOREIGN KEY (VolunteerID)
        REFERENCES VolunteerDetails(ID)
        ON DELETE CASCADE
) ENGINE = INNODB;