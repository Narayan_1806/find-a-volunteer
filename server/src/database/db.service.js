"use strict";
var _this = this;
var mysql = require("mysql");
var db = (function () {
    _this.pool = mysql.createPool({
        connectionLimit: 12,
        host: 'localhost',
        user: 'narayan1806',
        password: '',
        database: 'FindV',
        charset: 'utf8'
    });
    _this.getConnection = function (cb) {
        _this.pool.getConnection(cb);
    };
    _this.query = function (sql, values) { return new Promise(function (resolve, reject) {
        _this.pool.getConnection(function (err, connection) {
            if (err) {
                console.log(err);
            }
            else {
                connection.query(sql, values, function (err, results) {
                    connection.release();
                    if (err) {
                        console.log(err);
                    }
                    else {
                        resolve(results);
                    }
                });
            }
        });
    }); };
    return _this;
})();
module.exports = db;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRhdGFiYXNlL2RiLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLGlCQXFDWTtBQXJDWiw2QkFBZ0M7QUFFaEMsSUFBTSxFQUFFLEdBQUcsQ0FBQztJQUNYLEtBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQztRQUM1QixlQUFlLEVBQUUsRUFBRTtRQUNuQixJQUFJLEVBQUUsV0FBVztRQUNqQixJQUFJLEVBQUUsSUFBSTtRQUNWLFFBQVEsRUFBRSxVQUFVO1FBQ3BCLFFBQVEsRUFBRSxVQUFVO1FBQ3BCLE9BQU8sRUFBRSxNQUFNO0tBQ2YsQ0FBQyxDQUFDO0lBRUgsS0FBSSxDQUFDLGFBQWEsR0FBRyxVQUFDLEVBQU87UUFDNUIsS0FBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDN0IsQ0FBQyxDQUFDO0lBRUYsS0FBSSxDQUFDLEtBQUssR0FBRyxVQUFDLEdBQVEsRUFBRSxNQUFXLElBQUssT0FBQSxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1FBQ25FLEtBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQUMsR0FBUSxFQUFFLFVBQWU7WUFDakQsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDVCxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2xCLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDUCxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsVUFBQyxHQUFRLEVBQUUsT0FBWTtvQkFDcEQsVUFBVSxDQUFDLE9BQU8sRUFBRSxDQUFDO29CQUVyQixFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO3dCQUNULE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ2xCLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ1AsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUNsQixDQUFDO2dCQUNGLENBQUMsQ0FBQyxDQUFDO1lBQ0osQ0FBQztRQUNGLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQyxDQUFDLEVBaEJzQyxDQWdCdEMsQ0FBQztJQUVILE1BQU0sQ0FBQyxLQUFJLENBQUM7QUFDYixDQUFDLENBQUMsRUFBRSxDQUFDO0FBRUwsaUJBQVMsRUFBRSxDQUFDIiwiZmlsZSI6ImRhdGFiYXNlL2RiLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgbXlzcWwgPSByZXF1aXJlKCdteXNxbCcpO1xuXG5jb25zdCBkYiA9ICgoKSA9PiB7XG5cdHRoaXMucG9vbCA9IG15c3FsLmNyZWF0ZVBvb2woe1xuXHRcdGNvbm5lY3Rpb25MaW1pdDogMTIsXG5cdFx0aG9zdDogJ2xvY2FsaG9zdCcsXG5cdFx0dXNlcjogJ3NhJyxcblx0XHRwYXNzd29yZDogJyhJSk44dWhiJyxcblx0XHRkYXRhYmFzZTogJ3llbHBjYW1wJyxcblx0XHRjaGFyc2V0OiAndXRmOCdcblx0fSk7XG5cblx0dGhpcy5nZXRDb25uZWN0aW9uID0gKGNiOiBhbnkpID0+IHtcblx0XHR0aGlzLnBvb2wuZ2V0Q29ubmVjdGlvbihjYik7XG5cdH07XG5cblx0dGhpcy5xdWVyeSA9IChzcWw6IGFueSwgdmFsdWVzOiBhbnkpID0+IG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcblx0XHR0aGlzLnBvb2wuZ2V0Q29ubmVjdGlvbigoZXJyOiBhbnksIGNvbm5lY3Rpb246IGFueSkgPT4ge1xuXHRcdFx0aWYgKGVycikge1xuXHRcdFx0XHRjb25zb2xlLmxvZyhlcnIpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0Y29ubmVjdGlvbi5xdWVyeShzcWwsIHZhbHVlcywgKGVycjogYW55LCByZXN1bHRzOiBhbnkpID0+IHtcblx0XHRcdFx0XHRjb25uZWN0aW9uLnJlbGVhc2UoKTtcblxuXHRcdFx0XHRcdGlmIChlcnIpIHtcblx0XHRcdFx0XHRcdGNvbnNvbGUubG9nKGVycik7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdHJlc29sdmUocmVzdWx0cyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblx0XHRcdH1cblx0XHR9KTtcblx0fSk7XG5cblx0cmV0dXJuIHRoaXM7XG59KSgpO1xuXG5leHBvcnQgPSBkYjsiXX0=
