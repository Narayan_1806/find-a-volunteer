const models = require('../models');

module.exports = {

    async addEvent(req, res) {
        try 
        {
           await models.event.create
                (req.body).then(event => {
                    res.send ({
                        message : "Event added successfully",
                        success : true,
                        eventDetail: event
                    })
            });
        } catch (err) {
          res.status(500).send({
            error: 'An error has occured trying to add this event ' + err
          })
        }
    },

    async getEventsByStaffId(req, res) {
        try {
          await models.event.findAll({ where: {StaffID : req.query.StaffID,
                $and: { EventStatus: req.query.EventStatus } 
           }}).then(function(eventResults) {
                res.send(eventResults);
           });
        } catch (err) {
          res.status(500).send({
            error: 'An error has occured trying to retrieve the staff events' + err
          })
        }
    },
    
    async getEventResultsForStaff(StaffID, eventStatus) {
        var events = [];
        await models.event.findAll({ where: {StaffID : StaffID,
             $and: { EventStatus: eventStatus } 
        }}).then(function(eventResults) {
            events = eventResults;
        });
        return events;
    },

    // returns the array to display on the volunteer profile
    // TODO- put the current and past events in two arrays and send them
    async getVolunteerEventsByIDAndStatus(req, res) {
        try {
            res.send(getEventResultsForVolunteer(this.getVolunteerOrStaffId(2, req.body.userID), req.body.eventStatus));
        } catch (err) {
          res.status(500).send({
            error: 'An error has occured trying to retrieve the staff events' + err
          })
        }
    },

    async getEventResultsForVolunteer(volunteerID) {
        var events = [];
        await models.event.findAll({
            where : {},
            include: [
                {
                    model: models.eventapplications, 
                    where : { volunteerId :  volunteerID, $and : { IsIncluded: true } }
                }
            ]
        }).then(function(eventResults) {
            events = eventResults;
        });
        return events;
    },

    // returns the events which will be recommended based on
    // volunteer preferences given initially
    async getEventsForVolunteerDashboard(req, res) {
        try {
            var volunteerPreferences;
            var volunteerId = req.query.VolunteerId
    
            await models.volunteer.findOne({
                where: { id: volunteerId },
                attributes: ['LocationID']
            }).then(function(volunteerPreference) {
                if(volunteerPreference) {
                    models.event.findAll({
                        limit: 10,
                        where : { LocationID: volunteerPreference.LocationID, $and: { EventStatus: 'Open'}
                     }
                    }).then(function(eventResults) {
                        if(eventResults.length != 0) {
                            res.send({
                                message: "",
                                eventResults: eventResults
                            });
                        }
                        else {
                            models.event.findAll({
                                limit: 10
                            }).then(function(eventResults) {
                                res.send({
                                    message: "There are no events to apply for in your location. Check out some other events",
                                    eventResults: eventResults
                                });
                            });
                        }
                    });
                }
            })
        }
        catch(err) {
            console.log(err);
        }
    },

    async getAppliedVolunteers(req, res) {
        try{
            await models.eventapplication.findAll({
                where : { eventId : req.query.EventId },
                attributes: ['VolunteerName', 'Comments','volunteerId', 'IsVolunteerIncluded']
            }).then(function(appliedVolunteers){ 
                res.send(appliedVolunteers);
            });
        }
        catch(err) {
            res.status(500).send({
                error: 'An error has occured trying to retrieve the applied volunteers' + err
            })
        }
    },

    // TODO: Low Priority
    async getFilteredEventsForVolunteer() {

    },

    // TODO: Low Priority
    async getSearchResultsForVolunteer() {

    }

    // TODO: Low Priority - Write methods to update closed events
}