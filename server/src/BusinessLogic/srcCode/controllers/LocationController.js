const models = require('../models');

module.exports = {
     async getLocations(req, res) {
        try {
         await models.location.findAll().then(function(locations) {
                res.send(locations)
            });
        } catch (err) {
          res.status(500).send({
            error: 'An error has occured trying to retrieve the locations' + err
          })
        }
    },

    async addLocation(req, res) {
      try 
      {
          await models.location.create
                  (req.body).then(loc => {
                      res.send ({
                          message : "Location added successfully",
                          success : true,
                          location: loc
                      })
              });
      } catch (err) {
        res.status(500).send({
            error: 'An error has occured trying to add this location ' + err
        })
      }
  }
}