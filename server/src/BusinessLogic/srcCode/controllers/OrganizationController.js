const models = require('../models');

module.exports = {
     async getAllServices(req, res) {
        try {
         await models.service.findAll().then(function(services) {
                res.send(services)
            });
        } catch (err) {
          res.status(500).send({
            error: 'An error has occured trying to retrieve the services' + err
          })
        }
    },

    async addOrganization(req, res) {
        try 
        {
            // The below request is only a sample for testing
            // TODO - Priyanka - Needs to be removed
            if(!req.body) {
                req.body = 
                { 
                    Type : 'NGO', Name : 'TEACH FOR INDIA', 
                    Budget : 85000, Description : 'Mainly focuses on Education', 
                    StaffSize : 10000, ServiceId : 2
                };
            }
                await models.organization.create
                    (req.body).then(org => {
                        res.send ({
                            message : "Organization has been added successfully",
                            success : true
                        })
                });
                
        } catch (err) {
          res.status(500).send({
            error: 'An error has occured trying to add a new organization' + err
          })
        }
    }
}
