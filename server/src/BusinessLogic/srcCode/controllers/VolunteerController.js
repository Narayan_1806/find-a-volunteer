const models = require('../models');

module.exports = {

    async getVolunteerProfileDetails(req, res) {
        try 
        {
           await models.volunteer.findAll({
            where: { id: req.params.volunteerID },
            include: [
                {
                    model: models.user,
                    on: {
                        col1: models.sequelize.where(models.sequelize.col("volunteer.UserID"), "=", models.sequelize.col("user.id"))
                    },
                    attributes: ['Username','Email','PhoneNumber']
                }
            ]
        }).then((volunteerProfile) => {
            res.send(volunteerProfile);
        });
        } catch (err) {
          res.status(500).send({
            error: 'An error has occured while retrieving volunteer profile' + err
          })
        }
    }
}