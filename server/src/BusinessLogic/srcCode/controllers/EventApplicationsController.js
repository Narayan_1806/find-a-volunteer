const models = require('../models')
const EmailController = require('../controllers/EmailController')

module.exports = { 
     async applyForEvent(req, res) {
        try
        {
            await models.eventapplication.create
                    (req.body).then(event => {
                        res.send ({
                            message : "You have applied for this event successfully",
                            success : true,
                            eventDetail: event
                        })
                });
        } catch (err) {
            res.status(500).send({
                error: 'An error has occured trying to apply for this event ' + err
            })
        }
    },

    async approveEvent(req, res) {
        try 
        {
            var eventId = req.body.EventId;
            var volunteerIDs = req.body.VolunteerIds;

            models.sequelize.query("UPDATE eventapplications SET IsVolunteerIncluded = 1, IsNotificationSent = 1 " +
                                    "WHERE eventId = " + eventId + " AND " +
                                    " volunteerId IN (:volunteerIDS); " + 
                                    " UPDATE events SET EventStatus = 'Approved' WHERE id =" + eventId,
                { 
                    replacements: { volunteerIDS : volunteerIDs },
                    type: models.sequelize.QueryTypes.UPDATE }).then(result => {
                        if(result) {
                            res.send({
                                message : "Event has been approved successfully.",
                                success: true
                            })
                        }
                        EmailController.formMessageAndSendMails(eventId, volunteerIDs);
                });
        } catch (err) {
            res.status(500).send({
                error: 'An error has occured trying to approve this event ' + err
            })
        }
    }
}