const models = require('../models');

module.exports = {
    
    async getStaffProfileDetails(req, res) {
        try 
        {
           await models.staff.findOne({
            where: { id: req.params.staffID },
            include: [
                {
                    model: models.user,
                    on: {
                        col1: models.sequelize.where(models.sequelize.col("volunteer.UserID"), "=", models.sequelize.col("volunteer.id"))
                    },
                    attributes: ['Username','Email','PhoneNumber', 'NumberOfPastEvents' , 'NumberOfCurrentEvents']
                },
                {
                    model: models.organization,
                    on: {
                        col1: models.sequelize.where(models.sequelize.col("staff.OrganizationID"), "=", models.sequelize.col("organization.id"))
                    },
                    attributes: ['Name']
                }
            ]
        }).then((staffProfile) => {
            res.send({
               Name: staffProfile[0].user.Username,
               Email: staffProfile[0].user.Email,
               PhoneNumber: staffProfile[0].user.PhoneNumber,
               NumberOfCurrentEvents:  staffProfile[0].user.NumberOfCurrentEvents,
               NumberOfPastEvents: staffProfile[0].user.NumberOfPastEvents
            });
        });
        } catch (err) {
          res.status(500).send({
            error: 'An error has occured while retrieving volunteer profile' + err
          })
        }
    },

    async getStaffMembersForVolunteer(req, res) {
        try {
            var locId = req.query.LocationId;

            await models.staff.findAll({
                where : { LocationID : locId },
                include: [
                    {
                        model: models.user,
                        attributes: ['Username']
                    },
                ]
            }).then(staffMembers  => {
                res.send(staffMembers);
            })
        }catch(err) {
            res.status(500).send({
                error: 'An error has occured trying to retrieve the staff members' + err
            })
        }
    } 
}