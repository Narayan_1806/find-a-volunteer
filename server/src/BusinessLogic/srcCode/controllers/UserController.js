const models = require('../models');
const Promise = require('bluebird');
const bcrypt = Promise.promisifyAll(require('bcrypt-nodejs'));

module.exports = {
    async addUser(req, res) {
        try 
        {
           await models.user.findAll ({
                where : {
                     $or :  [{ Username : req.body.Username }, { Email : req.body.Email }]
                }
           }).then(userNameResp => {
               if(userNameResp.length == 0){
                    models.user.create
                        (req.body).then(userResp => {
                            res.send ({
                                message : "User created successfully",
                                success : true,
                                userDetails: userResp
                        })
                    });
               }
               else {
                    res.send ({
                        message : "Username/Email is already in use. Please check and try again.",
                        success : false
                    })
                }
           });
           
        } catch (err) {
          res.status(500).send({
            error: 'An error has occured trying to add a new user. Please contact the support team.'
          })
        }
    },

    async userLogin(req, res) {
        try{
            await models.user.findAll({
                where: { Username: req.body.Username },
                include: [{
                    model: models.staff,
                    on: {
                        col1: models.sequelize.where(models.sequelize.col("user.id"), "=", 
                        models.sequelize.col("staff.userId"))
                    },
                    attributes: ['id']
                },
                {
                    model: models.volunteer,
                    on: {
                        col1: models.sequelize.where(models.sequelize.col("user.id"), 
                        "=", models.sequelize.col("volunteer.userId"))
                    },
                    attributes: ['id']
                }]
            }).then(userResp => {

                bcrypt.compareAsync(req.body.Password, userResp[0].Password).
                then(data => {
                    if(data) {
                        if(userResp) {
                            res.send({
                               userDetails: userResp
                            })
                        }
                    }
                    else {
                            res.send({
                                message: "Username or password is incorrect. Please check and try again."
                            });
                    }
                });
                
            })
        } catch(err) {
            res.status(500).send({
                error: 'An error has occured trying to login. Please contact the support team.'
            }) 
        }
    },

    async addStaffDetail(req, res) {
        try 
        {
           await models.staff.create
                (req.body).then(staffResp => {
                    res.send ({
                        message : "Staff Details saved successfully",
                        success : true,
                        StaffDetails: staffResp
                    })
            });
        } catch (err) {
          res.status(500).send({
            error: 'An error has occured while saving staff details. Please contact the support team.'
          })
        }  
    },

    async addVolunteerDetail(req, res) {
        try 
        {
           await models.volunteer.create
                (req.body).then(volResp => {
                    res.send ({
                        message : "Volunteer details saved successfully",
                        success : true,
                        VolunteerDetails : volResp
                    })
            });
        } catch (err) {
          res.status(500).send({
            error: 'An error has occured while saving volunteer details. Please contact the support team.'
          })
        }
    }
}
