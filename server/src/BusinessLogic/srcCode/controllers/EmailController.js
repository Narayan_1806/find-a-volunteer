const models = require('../models')
const nodemailer = require('nodemailer')

module.exports = {
    
    async formMessageAndSendMails(eventId, volunteerIDs) {
        try {
            await models.event.findOne({
                where : { id : eventId },
                attributes : ['id','title', 'AcceptedMessage', 'RejectedReason']
            }).then(event => {
                this.constructMailString(event, volunteerIDs);
            })
        }catch(err) {
           console.log(err);
        }
    },

    constructMailString(event, volunteerIDs) {
        try {
            var acceptedMsg = event.AcceptedMessage;
            var rejectedMsg = event.RejectedReason;
    
            this.sendAcceptedEmail(event.title,acceptedMsg, volunteerIDs);
            this.sendRejectedEmail(event.title,rejectedMsg, event.id, volunteerIDs);
        }
        catch(err) {
          console.log(err)
        }
    },

    async sendAcceptedEmail(title, message, volunteerIDs) {
        try{
            await models.sequelize.query("SELECT GROUP_CONCAT(Email,',') AS 'AddressList' FROM users" +
            " WHERE id IN (SELECT userId FROM volunteers WHERE id IN (:volunteerIDS))",
            { 
                replacements: { volunteerIDS : volunteerIDs },
                type: models.sequelize.QueryTypes.SELECT }).then(result =>{
                this.sendEmail(title, result[0].AddressList, message); 
            });
        }
        catch(err) {
           console.log(err);
        }
    },

    async sendRejectedEmail(title, rejectedMsg, eventId, volunteerIDs) {
        try{
            await models.sequelize.query("SELECT GROUP_CONCAT(Email,',') AS 'AddressList' FROM users " +
            "WHERE id IN (SELECT userId FROM volunteers WHERE id IN " +
                "(SELECT volunteerId FROM eventapplications WHERE eventId = " + eventId + " AND " +
                 "IsVolunteerIncluded = 0 AND IsNotificationSent = 0))",
            { 
                replacements: { volunteerIDS : volunteerIDs },
                type: models.sequelize.QueryTypes.SELECT }).then(result =>{
                this.sendEmail(title, result[0].AddressList, rejectedMsg); 
            });
        }
        catch(err) {
           console.log(err);
        }
    },

    sendEmail(title, toEmailAddress, message) {

        var fromEmailAddress = 'madirajupriyanka@gmail.com';

        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
              user: fromEmailAddress,
              pass: 'MyGoogle$7'
            }
          });
          
          var mailOptions = {
            from: fromEmailAddress,
            to: toEmailAddress,
            subject: title + ' Event Application Status',
            text: message
          };
          
          transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
            }
          });
    }
}
 