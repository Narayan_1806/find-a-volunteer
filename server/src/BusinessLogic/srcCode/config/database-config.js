const path = require('path')

module.exports = {
  port: process.env.PORT || 3036,
  db: {
    database: process.env.DB_NAME || 'FindAVolunteer',
    user: process.env.DB_USER || 'root',
    password: process.env.DB_PASS || '9803110824',
    options: {
      dialect: process.env.DIALECT || 'mysql',
      host: process.env.HOST || 'localhost',
      dialectOptions: {
          multipleStatements: true
      }
    }
  }
}
