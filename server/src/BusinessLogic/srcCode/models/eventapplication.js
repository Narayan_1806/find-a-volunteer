module.exports = (sequelize, DataTypes) => {
    const eventapplication = sequelize.define('eventapplication', {
        VolunteerName: {
            type: DataTypes.STRING(300),
            allowNulls: false
        },
        Comments: DataTypes.TEXT,
        IsVolunteerIncluded: DataTypes.BOOLEAN,
        IsNotificationSent: DataTypes.BOOLEAN
    }
)

    return eventapplication
}