var models = require('../models');
module.exports = (sequelize, DataTypes) => {
    const role = sequelize.define('role', {
        Name: {
            type : DataTypes.STRING,
            unique : true,
            allowNulls : false
        }
    });
    return role
}