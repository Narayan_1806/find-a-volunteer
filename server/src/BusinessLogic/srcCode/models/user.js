var models = require('../models');
const Promise = require('bluebird');
const bcrypt = Promise.promisifyAll(require('bcrypt-nodejs'));

module.exports = (sequelize, DataTypes) => {
    const user = sequelize.define('user', {
        Username: {
            type : DataTypes.STRING,
            unique : true,
            allowNulls : false
        },
        Email: {
            type: DataTypes.STRING,
            unique: true,
            allowNulls: false,
            validate : {
                isEmail: true
            }
        },
        PhoneNumber: {
            type: DataTypes.STRING,
            allowNulls: false
        },
        Password: {
            type: DataTypes.TEXT,
            allowNulls: false
        },
        IsEmailVerified : {
            type : DataTypes.BOOLEAN,
            defaultValue: false
        },
        IsGoogleAuth : {
            type : DataTypes.BOOLEAN,
            defaultValue: false
        },
        IsAccountActive : {
            type : DataTypes.BOOLEAN,
            defaultValue: true
        },
        RoleId : {
           type: DataTypes.INTEGER,
           allowNulls: false
        },
        NumberOfPastEvents: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        NumberOfCurrentEvents:{
            type: DataTypes.INTEGER,
            defaultValue: 0            
        }
    },
    {   
        hooks: {
          beforeCreate: function hashPassword (user, options) {
            const SALT_FACTOR = 8

            return bcrypt
              .genSaltAsync(SALT_FACTOR)
              .then(salt => bcrypt.hashAsync(user.dataValues.Password, salt, null))
              .then(hash => {
                    user.Password = hash
              })
          }
        }
    })
    
    user.associate = function (models) {
        user.belongsTo(models.role, {
            foreignKey: 'RoleId'
        })

        user.hasOne(models.volunteer);
        user.hasOne(models.staff);
    }

    return user
}

