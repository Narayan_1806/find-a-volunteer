var models = require('../models');
module.exports = (sequelize, DataTypes) => {
    const location = sequelize.define('location', {
        Name: {
            type : DataTypes.STRING,
            unique : true,
            allowNulls : false
        },
        State : DataTypes.STRING(400),
        latitude: DataTypes.INTEGER,
        longitude: DataTypes.INTEGER
    });

    return location
}