module.exports = (sequelize, DataTypes) => {
    const volunteer = sequelize.define('volunteer', {
        LocationID: {
            type: DataTypes.INTEGER,
            allowNulls: false
        },
        StaffID: 
        {
            type: DataTypes.INTEGER,
            allowNulls: false
        },
        IsPromoted: {
           type: DataTypes.BOOLEAN,
           defaultValue: false
        },
        AvgRating: DataTypes.FLOAT(2,2),
        PreferredDays: {
            type: DataTypes.ENUM,
            values: ['WeekDays', 'Weekend']
        },
        PreferredStartTime: DataTypes.TIME,
        PreferredEndTime: DataTypes.TIME,
        WorkExperience: DataTypes.TEXT,
        TypeOfVolunteeringInterested: {
            type: DataTypes.ENUM,
            values: ['Online', 'Offline'] 
        },
    }
)

volunteer.associate = function (models) {
  
    volunteer.belongsTo(models.location, {
        foreignKey: 'LocationID'
    })
    volunteer.belongsTo(models.staff, {
        foreignKey: 'StaffID'
    })

    volunteer.belongsToMany(models.event, { through: 'eventapplication' })}
    return volunteer
}