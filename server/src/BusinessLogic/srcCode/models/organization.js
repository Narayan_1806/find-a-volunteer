
module.exports = (sequelize, DataTypes) => {
    const organization = sequelize.define('organization', {
        Type: DataTypes.STRING,
        Name: {
            type : DataTypes.STRING,
            unique : true,
            allowNulls : false
        },
        Budget : DataTypes.DECIMAL(10,2),
        Description : DataTypes.TEXT,
        StaffSize : DataTypes.INTEGER
    }
)

return organization
}