module.exports = (sequelize, DataTypes) => {
    const staff = sequelize.define('staff', {
        LocationID: DataTypes.INTEGER,
    }
)

staff.associate = function (models) {
    staff.belongsTo(models.location, {
        foreignKey: 'LocationID'
    })

    staff.belongsTo(models.user)
}

return staff
}