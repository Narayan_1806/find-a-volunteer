module.exports = (sequelize, DataTypes) => {
    const event = sequelize.define('event', {
        title: {
            type: DataTypes.TEXT,
            allowNulls: false
        },
        Description: {
            type: DataTypes.TEXT,
            allowNulls: false
        },
        LocationID: {
            type: DataTypes.INTEGER,
            allowNulls:false
        },
        StartDateTime: {
            type: DataTypes.DATE,
            allowNulls: false
        },
        EndDateTime: {
            type: DataTypes.DATE,
            allowNulls: false
        },
        NumOfVolunteersNeeded: {
            type: DataTypes.INTEGER
        },
        EventStatus: {
            type: DataTypes.ENUM,
            values: ['Open', 'Live', 'Approved', 'Closed', 'Reopened'],
            defaultValue: 'Open'
        },
        StaffID: {
            type: DataTypes.INTEGER,
            allowNulls: false
        },
        RejectedReason: {
            type: DataTypes.TEXT,
            defaultValue: 'Sorry! Your application for this event has been rejected'
        },
        AcceptedMessage: {
            type: DataTypes.TEXT,
            defaultValue: 'Congrats! Your application for this event has been selected.Please wait for further communication'
        },
        ShouldBeFinalizedIn: {
            type: DataTypes.INTEGER,
            defaultValue: 10
        },
        TypeOfEvent: {
            type: DataTypes.ENUM,
            values: ['Online', 'Offline'] 
        },
        EventDays: {
            type: DataTypes.ENUM,
            values: ['Weekend', 'Weekday'] 
        }
    }
)

    event.associate = function (models) {
        event.belongsTo(models.location, {
            foreignKey: 'LocationID'
        })
        event.belongsTo(models.staff, {
            foreignKey: 'StaffID'
        })
        event.belongsTo(models.staff, {
            foreignKey: 'StaffID'
        })

        event.belongsToMany(models.volunteer, { through: 'eventapplication' })
    }

    return event
}