const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const {sequelize} = require('./models')
const config = require('./config/server-config')

const app = express()

app.use(morgan('combined')) // Used to print logs for debugging purposes
app.use(bodyParser.json())
app.use(cors()) // Security risk

require('./route/allroutefiles')(app)

sequelize.sync({force: false})
  .then(() => {
    app.listen(config.serverport)
    console.log(`Server started on port ${config.serverport}`)
})