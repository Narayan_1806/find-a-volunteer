const OrganizationController = require('../controllers/OrganizationController')
const LocationController = require('../controllers/LocationController')
const UserController = require('../controllers/UserController')
const VolunteerController = require('../controllers/VolunteerController')
const EventController = require('../controllers/EventController')
const StaffController = require('../controllers/StaffController')
const EventApplicationsController = require('../controllers/EventApplicationsController')

module.exports = (app) => {
    app.get('/getServices',OrganizationController.getAllServices)

    app.post('/addUser', UserController.addUser)
    app.post('/userLogin', UserController.userLogin)
    app.post('/addStaffDetail', UserController.addStaffDetail)
    app.post('/addVolunteerDetail', UserController.addVolunteerDetail)

    app.post('/addOrganization', OrganizationController.addOrganization)
    app.get('/getLocations', LocationController.getLocations)
    app.post('/addLocation', LocationController.addLocation)

    app.get('/getVolunteerProfileDetails/:volunteerID', VolunteerController.getVolunteerProfileDetails)

    app.post('/addEvent', EventController.addEvent)
    app.get('/getEventsByStaffIdAndStatus', EventController.getEventsByStaffId)
    app.get('/getVolunteerEventsByIDAndStatus', EventController.getVolunteerEventsByIDAndStatus)

    app.get('/getAppliedVolunteers', EventController.getAppliedVolunteers)
    app.post('/applyForEvent', EventApplicationsController.applyForEvent)
    app.post('/approveEvent', EventApplicationsController.approveEvent)

    app.get('/getEventsForVolunteerDashboard/', EventController.getEventsForVolunteerDashboard)
    app.get('/getStaffMembersForVolunteer/', StaffController.getStaffMembersForVolunteer)
}