import { Component, Directive } from '@angular/core';
import { DataService } from '../services/data.service';
import { EventcardComponent } from '../eventcard/eventcard.component';
import { Events } from '../models/events';
import { EventService } from '../services/event.service';

@Component({
  selector: 'app-event-root',
  templateUrl: 'events.component.html',
  styleUrls: ['events.component.css']
})
export class EventsComponent {

  title = 'Events';
  isAddEventButtonVisible = false;
  events: Events[];
  statusMessages: string;

  constructor(private dataservice : DataService, private eventService: EventService) { 
    // show the button only for staff members
    if(this.dataservice) {
      this.isAddEventButtonVisible =  this.dataservice.userState.isVisibleToStaff;
      if(this.isAddEventButtonVisible) {
        this.getOpenEvents();
      }
      else {
        this.getEventsForVolunteer();
      }
    }
  }

  getOpenEvents() {
     this.eventService.getEventsForStaff(this.dataservice.userState.StaffId, 'Open').subscribe(data =>
      this.assignToEvents(data));
  }

  getClosedEvents() {
    this.eventService.getEventsForStaff(this.dataservice.userState.StaffId, 'Approved').subscribe(data =>
      this.assignToEvents(data));
  }

  getEventsForVolunteer() {
    this.eventService.getEventsForVolunteer(this.dataservice.userState.VolunteerId).subscribe(data =>
      this.assignToEventsVolunteer(data));
  }

  assignToEvents(data) {
    this.events = data;
  }

  assignToEventsVolunteer(data) {
    this.statusMessages = data.message;
    this.events = data.eventResults;
  }
}
