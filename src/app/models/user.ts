export interface User {
    Username: string,
    Email: string,
    Password: string,
    PhoneNumber: string,
    RoleId: number
}