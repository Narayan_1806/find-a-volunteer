export class Staff {
    id: number;
    name: string;
    image: string;
    email:string;
    contact_number:number;
    HeadOfLocation : string;
    lat:number;
    org_name:string;
    CreatedDateTime:string;
    LastModifiedDateTime:string;
    description: string;
}
