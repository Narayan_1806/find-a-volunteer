export interface Events {
    id: number
    title: string
    LocationID: number
    Description : string
    StartDateTime: String
    EndDateTime: String
    NumOfVolunteersNeeded: number
    StaffID: number
    TypeOfEvent: String
    EventDays: String,
    RejectedReason: String,
    AcceptedMessage: String,
    EventStatus: String
}