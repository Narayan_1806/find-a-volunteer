export interface Location {
    id: number
    Name: String
    State: String
    latitude: number
    longitude: number
}