import { Component, OnInit } from '@angular/core';
import { EventService } from '../services/event.service';
import { DataService } from '../services/data.service';
import { LocationService } from '../services/location.service';
import { StaffService } from '../services/staff.service';
import { Events } from '../models/events';
import { Location } from '../models/location';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-preference',
  templateUrl: './user-preference.component.html',
  styleUrls: ['./user-preference.component.css']
})
export class UserPreferenceComponent implements OnInit {

  locations: Location[]
  selectedLocation: Location

  selectedTypeOfEvent:any
  selectedEventDay: any

  typeOfEvents: any[]
  EventDays: any[]

  isVisibleToStaff: boolean

  staffMembers: any[]
  selectedStaffMember: any

  selectedStartTime: any
  selectedEndTime: any

  timings: any[]

  statusMessages : string;

  constructor(private eventService: EventService, private dataService: DataService, 
    private router: Router, private locationService: LocationService, private staffService: StaffService,
    private userService: UserService) {
   }

  ngOnInit() {
    this.isVisibleToStaff = this.dataService.userState.isVisibleToStaff;
    this.locationService.getLocations().subscribe(data => this.assignLocations(data));
    this.typeOfEvents = [{value : 'Online'}, {value: 'Offline'}]
    this.EventDays = [{value : 'Weekend'}, { value : 'Weekdays'}]
    this.timings= [{value: '09:00'}, { value : '10:00'}, { value : '11:00'}, { value : '12:00'},
    { value : '13:00'}, { value : '14:00'}, { value : '15:00'}, { value : '16:00'},
    { value : '17:00'}, { value : '18:00'}, {value: '19:00'}]
  }

 assignLocations(data) {
   this.locations = data;
 }

 locChanged(locId) {
    this.staffService.getStaffMembersForVolunteer(this.selectedLocation.id).subscribe(data => this.assignStaffMembers(data));
 }

 assignStaffMembers(data) {
   this.staffMembers = data;
 }

 saveUserPref() {
    if(this.isVisibleToStaff) {
        this.addStaffDetail();
    }
    else {
        this.addVolunteerDetail();
    }
 }

 addStaffDetail() {
    this.userService.addStaffDetail({
      LocationID: this.selectedLocation.id,
      userId: this.dataService.userState.UserId
    }).subscribe(data => this.moveToLoginPage(data));
 }

 addVolunteerDetail() {
  this.userService.addVolunteerDetail({
      LocationID: this.selectedLocation.id,
      StaffID: this.selectedStaffMember.id,
      PreferredDays: this.selectedEventDay.value,
      TypeOfVolunteeringInterested: this.selectedTypeOfEvent.value,
      PreferredStartTime: this.selectedStartTime.value,
      PreferredEndTime: this.selectedEndTime.value,
      userId: this.dataService.userState.UserId
  }).subscribe(data => this.moveToLoginPage(data));
 }

   moveToLoginPage(data) {
     if(data.success) {
       this.statusMessages = data.message + ".Redirecting to Login page."
       this.router.navigate(['login']);
     }
     else {
        this.statusMessages = data.error;
     }
  }
}
