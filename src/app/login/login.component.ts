import { Component } from '@angular/core';
import { UserService } from '../services/user.service';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-root',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent {
  title = 'Login as a Volunteer';
  roleId = 0;

  statusMessages: ""

  userCredentials: UserCredentials = {
    Username: '', Password: ''
  }

  constructor(private userService: UserService, private router: Router, private dataservice : DataService) {
  }

  userLogin() {
      this.userService.loginUser(this.userCredentials).subscribe(data =>  
        this.redirectToEventsPage(data)
      )
  }

  redirectToEventsPage(data) {

     if(data.message) {
        this.statusMessages = data.message;
        return;
     }
     else if(data.error) {
       this.statusMessages = data.error;
       return;
     }

      var roleId = data.userDetails[0].RoleId;
      var userId = data.userDetails[0].id;

      this.dataservice.userState = {
          RoleId : roleId,
          UserId : userId,
          isVisibleToStaff: (roleId == 1),
          StaffId:  data.userDetails[0].staff? data.userDetails[0].staff.id: -1,
          VolunteerId: data.userDetails[0].volunteer? data.userDetails[0].volunteer.id : -1,
          Username: data.userDetails[0].Username
      }

      this.router.navigate(['events']);
  }
}

interface UserCredentials {
    Username: String,
    Password: String
}
