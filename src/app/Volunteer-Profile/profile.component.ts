import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Events } from '../models/events';
import { EventService } from '../services/event.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LocationService } from '../services/location.service';

@Component({
  selector: 'app-profile-root',
  templateUrl: 'profile.component.html',
  styleUrls: ['profile.component.css']
})
export class ProfileComponent implements OnInit {

  currentVolId : number;

  constructor(private dataService: DataService, private eventService: EventService,
    private router: Router, private route: ActivatedRoute, private locService: LocationService) {
      this.currentVolId = + this.route.snapshot.queryParamMap.get('volId');
  }

  ngOnInit() {

  }
}
