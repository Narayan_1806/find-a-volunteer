import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataService } from '../services/data.service';
import { Events } from '../models/events';
import { EventService } from '../services/event.service';
import { Router } from '@angular/router';
import { LocationService } from '../services/location.service';

@Component({
  selector: 'app-eventdetail',
  templateUrl: './eventdetail.component.html',
  styleUrls: ['./eventdetail.component.css']
})

export class EventdetailComponent implements OnInit {

  statusMessages: string;
  currentEvent: Events;
  isVisibleToStaff : boolean;
  Comments: String;
  appliedVolunteers: any[];
  includedVolunteerIds : number[] = [];
  locName: any
  volalreadyApplied: boolean;
  showApplyControls: boolean;
  showApproveControls: boolean;
  
  constructor(private dataService: DataService, private eventService: EventService,
          private router: Router, private locService: LocationService,
          private ref: ChangeDetectorRef) {
        
       if(dataService) {
          this.currentEvent =  dataService.selectedEvent;
          this.isVisibleToStaff = dataService.userState.isVisibleToStaff;
          this.getAppliedVolunteers();
          this.toggleApproveControls();
       }
       this.locService.getLocations().subscribe(data => this.setLocationName(data));
  }

  applyEvent() {
      return this.eventService.applyEvent({
          eventId : this.currentEvent.id,
          volunteerId: this.dataService.userState.VolunteerId,
          Comments : this.Comments,
          VolunteerName: this.dataService.userState.Username
      }).subscribe(data => {
          if(data.success) {
            this.statusMessages = data.message;
            this.getAppliedVolunteers();
            this.toggleApplyControls();
            this.ref.detectChanges();
          }
          else {
              this.statusMessages = data.error;
          }
      });
  }

  getAppliedVolunteers() {
      this.eventService.getAppliedVolunteers(this.currentEvent.id).subscribe(data => 
        this.assignVolunteers(data))
  }

  assignVolunteers(data){
    this.appliedVolunteers = data;
    this.toggleApplyControls();
  }

  OnCheckboxSelect(volId, event) {
    if(this.includedVolunteerIds.indexOf(volId) === -1 && event.target.checked)
    {
        this.includedVolunteerIds.push(volId);
    }
    else if(!event.target.checked)
    {
        let index = this.includedVolunteerIds.indexOf(volId);
        this.includedVolunteerIds.splice(index, 1);
    }
  }

  setLocationName(allLocs) {
    var currEvent = this.currentEvent;
    var selectedLoc = allLocs.filter(val => {
        return val.id === currEvent.LocationID
     })[0];
     this.locName = selectedLoc.Name + "," + selectedLoc.State  + ",India"
  }

  checkIfCurrentVolAlreadyApplied() {
     if(this.appliedVolunteers) {
        this.volalreadyApplied = this.appliedVolunteers.filter(val => {
            return val.volunteerId == this.dataService.userState.VolunteerId
        }).length > 0;
     }
     else {
         this.volalreadyApplied = false;
     }
  }

  toggleApplyControls() {
    this.checkIfCurrentVolAlreadyApplied();
    this.showApplyControls = (!this.isVisibleToStaff && !this.volalreadyApplied);
  }

  toggleApproveControls() {
     this.showApproveControls = (this.isVisibleToStaff && this.currentEvent.EventStatus != 'Approved')
  }

  approveEvent() {
      this.currentEvent.EventStatus = 'Approved';
      this.toggleApproveControls();
      this.ref.detectChanges();
      this.eventService.approveEvent(this.currentEvent.id, this.includedVolunteerIds).subscribe
      (data =>  {
          if(data.success){
            this.statusMessages = data.message;
          }
          else {
            this.statusMessages = data.error;
          }
      });
  }

  ngOnInit() {
  }
  
}
