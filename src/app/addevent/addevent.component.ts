import { Component, OnInit } from '@angular/core';
import { EventService } from '../services/event.service';
import { DataService } from '../services/data.service';
import { LocationService } from '../services/location.service';
import { Events } from '../models/events';
import { Location } from '../models/location';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addevent',
  templateUrl: './addevent.component.html',
  styleUrls: ['./addevent.component.css']
})
export class AddeventComponent implements OnInit {

  statusMessages: string;

  event: Events = {
    id: 0,
    title: '',
    LocationID: 0,
    Description: '',
    StartDateTime : Date.now.toString(),
    EndDateTime: Date.now.toString(),
    NumOfVolunteersNeeded: 0,
    StaffID: 0,
    TypeOfEvent: '',
    EventDays: '',
    RejectedReason: '',
    AcceptedMessage : '',
    EventStatus: 'Open'
  };

  locations: Location[]
  selectedLocation: Location

  selectedTypeOfEvent:any
  selectedEventDay: any

  typeOfEvents: any[]
  EventDays: any[]

  constructor(private eventService: EventService, private dataService: DataService, 
              private router: Router, private locationService: LocationService) {
  }

  ngOnInit() {
    this.locationService.getLocations().subscribe(data => this.assignLocations(data));
    this.typeOfEvents = [{value : 'Online'}, {value: 'Offline'}]
    this.EventDays = [{value : 'Weekend'}, { value : 'Weekday'}]
  }

  addEvent() {
     this.event.LocationID = this.selectedLocation.id;
     this.event.StaffID = this.dataService.userState.StaffId;
     this.event.TypeOfEvent = this.selectedTypeOfEvent.value;
     this.event.EventDays = this.selectedEventDay.value;
     this.eventService.addEvent(this.event).subscribe(data => this.moveBackToEventsPage(data));
  }

  assignLocations(data) {
    this.locations = data;
  }

  moveBackToEventsPage(data) {
    if(data.success) {
      this.router.navigate(['events']);
    }
    else {
      this.statusMessages = "An error occured while adding the event";
    }
  }
}
