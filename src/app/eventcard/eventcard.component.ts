import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { Events } from '../models/events';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-eventcard',
  templateUrl: './eventcard.component.html',
  styleUrls: ['./eventcard.component.css']
})
export class EventcardComponent implements OnInit {

  statusMessages: string;

  @Input() events: Events[];

  constructor(private dataService: DataService, private router: Router) { 
  }

  ngOnInit() {
  }

  showEventDetail($event: any) {
    this.dataService.selectedEvent = $event;
    this.router.navigate(['eventdetail']);
  }

}