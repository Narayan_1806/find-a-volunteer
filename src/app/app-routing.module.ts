import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { EventsComponent } from './events/events.component';
import { EventdetailComponent } from './eventdetail/eventdetail.component';
import { AddeventComponent } from './addevent/addevent.component';
import { UserPreferenceComponent } from './user-preference/user-preference.component';
import { ProfileComponent } from './Volunteer-Profile/profile.component';

export const AppRoutes: Routes = [
    {
        path: '',
        component: LoginComponent, 
        pathMatch: 'full' 
    },
    {
        path: 'signup', 
        component: SignupComponent, 
        pathMatch: 'full' 
    },
    {
        path: 'login', 
        component: LoginComponent
    },
    {
        path: 'events',
        component: EventsComponent
    },
    {
        path: 'eventdetail',
        component: EventdetailComponent
    },
    {
        path: 'addEvent',
        component: AddeventComponent
    },
    {
        path: 'userpref',
        component: UserPreferenceComponent
    },
    {
        path: 'volProfile',
        component : ProfileComponent,
        pathMatch: 'full'
    }
]

export class AppRoutingModule {}