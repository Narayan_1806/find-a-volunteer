import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule, Http } from '@angular/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SidebarModule } from 'ng-sidebar';
import { AgmCoreModule } from '@agm/core';

import {AppRoutes} from './app-routing.module';
import { AppRoutingModule } from './app-routing.module';

// Components
import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { EventsComponent } from './events/events.component';
import { EventdetailComponent } from './eventdetail/eventdetail.component';
import { EventcardComponent } from './eventcard/eventcard.component';
import { AddeventComponent } from './addevent/addevent.component';
import { AddOrganisationComponent } from './add-organisation/add-organisation.component';
import { UserPreferenceComponent } from './user-preference/user-preference.component';
import { StaffComponent } from './staff/staff.component';
import { ProfileComponent } from './Volunteer-Profile/profile.component';

// Services
import { ApiService } from './services/api.service';
import { UserService } from './services/user.service';
import { DataService } from './services/data.service';
import { EventService } from './services/event.service';
import { LocationService } from './services/location.service';
import { StaffService } from  './services/staff.service';

// PWA Related Dependencies
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    EventsComponent,
    EventcardComponent,
    AddeventComponent,
    EventdetailComponent,
    AddOrganisationComponent,
    UserPreferenceComponent,
    StaffComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(AppRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ServiceWorkerModule.register('/ngsw-worker.js', {enabled: true})
  ],
  providers: [
    ApiService,
    UserService,
    DataService,
    EventService,
    LocationService,
    StaffService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
