import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-signup-root',
  templateUrl: 'signup.component.html',
  styleUrls: ['signup.component.css']
})
export class SignupComponent implements OnInit {

  statusMessages : '';
  title = 'User Signup';
  user: User = { Username: '', Email: '', Password : '', PhoneNumber: '', RoleId: 0 };

  constructor(private userService: UserService, private router: Router, private dataService : DataService) {
  }

  ngOnInit() : void {
  }
  
  userSignup() {
    var respData = this.userService.addUser(this.user).subscribe(data => this.assignUserState(data));
  }

  assignUserState(data) {
    if(data.success) {
      var roleId = data.userDetails.RoleId;
      var userId = data.userDetails.id;
  
      this.dataService.userState = {
          RoleId : roleId,
          UserId : userId,
          isVisibleToStaff: (roleId == 1),
          StaffId:  data.userDetails.staff? data.userDetails[0].staff.id: -1,
          VolunteerId: data.userDetails.volunteer? data.userDetails[0].volunteer.id : -1,
          Username: data.userDetails.Username
      }
  
      this.router.navigate(['userpref']);
    }
    else {
       this.statusMessages = data.message ? data.message : data.error
    }
  }
}
