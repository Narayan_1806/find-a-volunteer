import { Injectable } from '@angular/core';
import { Events } from '../models/events';
 
@Injectable() 
export class DataService {
    userState: UserState;
    selectedEvent: Events;
}

interface UserState {
    RoleId : number,
    UserId : number,
    isVisibleToStaff: boolean,
    StaffId: number,
    VolunteerId: number,
    Username: String
}