import { ApiService } from '../services/api.service';
import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class StaffService {

    constructor(private apiService : ApiService) {
    }

    getStaffMembersForVolunteer(locId) {
        return this.apiService.getMethods('getStaffMembersForVolunteer', {
            LocationId : locId
        });
    }
 }