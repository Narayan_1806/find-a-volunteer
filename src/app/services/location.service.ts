import { ApiService } from '../services/api.service';
import { Injectable } from '@angular/core';
import { Location } from '../models/location';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LocationService {

    constructor(private apiService : ApiService) {
    }

    getLocations() {
       return this.apiService.getMethods('getLocations', {});
    }
}