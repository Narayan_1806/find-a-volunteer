import { Injectable } from '@angular/core';
import { Headers,Http, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ApiService {

    apiUrl : String = 'http://localhost:8081/';
    postMethodResult: any;

    constructor(private _http: Http) {
    }

    postMethods(methodName, dataToPost) : any {

        const headers =  new Headers();
        headers.append('Content-Type', 'application/json');
        const url = this.apiUrl + methodName;

        var resp = this._http.post(url, JSON.stringify(dataToPost), { headers })
        .map((res) =>  {
            return res.json();
        });
        return resp;
    }

    getMethods(methodName, params) {

        const url = this.apiUrl + methodName;

        var paramsToSend : URLSearchParams = new URLSearchParams();
        paramsToSend = params;

        var resp = this._http.get(url, {
            search : paramsToSend
        })
        .map((res) =>  {
            return res.json();
        });
        return resp;
    }
}

