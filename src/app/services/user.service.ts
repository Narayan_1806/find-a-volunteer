import { ApiService } from '../services/api.service';
import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {

    constructor(private apiService : ApiService) {
    }

    addUser(user) {
       return this.apiService.postMethods('addUser', user);
    }

    loginUser(userCredentials) {
        return this.apiService.postMethods('userLogin', userCredentials);
    }

    addVolunteerDetail(volDetail) {
        return this.apiService.postMethods('addVolunteerDetail', volDetail);
    }

    addStaffDetail(staffDetail) {
        return this.apiService.postMethods('addStaffDetail', staffDetail);
    }
 }