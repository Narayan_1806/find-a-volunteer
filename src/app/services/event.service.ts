import { ApiService } from '../services/api.service';
import { Injectable } from '@angular/core';
import { Events } from '../models/events';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class EventService {

    constructor(private apiService : ApiService) {
    }

    addEvent(event) : Observable<any> {
        return this.apiService.postMethods('addEvent', event);
    }

    getEventsForVolunteer(volunteerID) : Observable<any> {
        return this.apiService.getMethods('getEventsForVolunteerDashboard',{
            VolunteerId : volunteerID
        });
    }

    getEventsForStaff(staffId, status) : Observable<any> {
        return this.apiService.getMethods('getEventsByStaffIdAndStatus', {
            StaffID: staffId,
            EventStatus: status
        });
    }

    applyEvent(eventApplication) {
        return this.apiService.postMethods('applyForEvent', eventApplication);
    }

    getAppliedVolunteers(eventId) {
        return this.apiService.getMethods('getAppliedVolunteers', {
            EventId : eventId
        });
    }

    approveEvent(eventId, volunteerIds : number[]) {
        return this.apiService.postMethods('approveEvent', {
            EventId : eventId,
            VolunteerIds: volunteerIds
        });
    }
    
}